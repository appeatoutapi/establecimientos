//obtenemos el modelo UserModel con toda la funcionalidad
var UserModel = require('../models/cities');
var ModelUser = require('../models/ofertas');
var ModelEvents = require ('../models/events.js');
var ModelFav = require ('../models/fav.js');
var ModelNear= require ('../models/dondeIr.js');
var ModelEstablishment = require ('../models/establishment.js');
var ModelProducts= require ('../models/carta.js');
var ModelMenu = require ('../models/menu.js');
var ModelReviews = require ('../models/reviews.js');
var ModelFilters = require ('../models/filters.js');
var ModelFilterAdvanced = require ('../models/filterAdvanced.js');
var ModelUserData = require ('../models/users.js');

var error = new Array();

//creamos el ruteo de la aplicaci�n
module.exports = function(app)
{

  
 //Actualiza imagen del usuario
    app.post("/updateAvatar", function(req,res)
    {
        //creamos un objeto con los datos a insertar
        var userData = {            
            image : req.body.image,
            user_id : req.body.id          
        };
        ModelUserData.updateImage(userData,function(error, data)
        {
           //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            
            
        });
    })

 
    //mostramos todas las ciudades 
    app.get("/cities", function(req,res){
        UserModel.getUsers(function(error, data)
        {
            res.json(204,null);
        });
    });
 
 //obtiene los datos de contacto por su id
    app.get("/establishment/contacts/:id", function(req,res)
    {
       var resposta;      
           //id del usuario
     if (!isNaN(req.params.id)){
        var id = req.params.id;        
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelEstablishment.getContact(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });


 //mostramos todas las ciudades 
    app.get("/ComproveCities", function(req,res){
        UserModel.getCities(function(error, data)
        {
            res.json(200,data);
        });
    });

    //obtiene una ciudad por su nombre
    app.get("/cities/:nombre", function(req,res)
    {
        //id del usuario
        if(isNaN(req.params.nombre)){
        var nombre = "%"+req.params.nombre+"%";
    }  if (!isNaN(req.params.nombre)){
        var nombre = req.params.nombre;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(nombre !=null)
       {
            UserModel.getUser(nombre,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });


    //mostramos todas las ciudades 
    app.get("/ComproveCities", function(req,res){
        UserModel.getCities(function(error, data)
        {
            res.json(200,data);
        });
    });

    //obtiene los datos de un usuario
    app.get("/getUserdata/:id", function(req,res)
    {
       if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        
       if(id !=null)
       {
            ModelUserData.getUser(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

       //obtiene los datos de un usuario
    app.get("/historyGetData/:type/:id", function(req,res)
    {
       if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
    if(!isNaN(req.params.type)){
      var type= req.params.type;
    }
        
       if((id !=null) && (type !=null))
       {
            ModelFav.getOfferOrEvent(type,id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });



    //obtiene las categor�as de un establecimiento
    app.get("/products/categories/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelProducts.getCategories(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

 //obtiene los productos de un establecimiento
    app.get("/products/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelProducts.getUser(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

 //obtiene los comentarios de un establecimiento
    app.get("/reviews/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelReviews.getUser(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         data=[{"msg":"0"}];
         res.json(500,data);
       }
    });



 //obtiene los datos del usuario
    app.get("/dataUser/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelReviews.getData(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

 /*obtiene los eventos que va a ir un usuario
    app.get("/wanttogo/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        if(id !=null)
       {

            ModelGo.getUser(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                   res.json(200,data);
                    
                                     
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,{"msg":"No favorite events"});
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         res.json(500,{"msg":"Error"});
       }
    });

 //obtiene los usuariso que van a ir a un evento
    app.get("/wanttogo/event/:id", function(req,res)
    {
                //id delestablecimiento
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        if(id !=null)
       {

            ModelGo.getEvent(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                   res.json(200,data);
                    
                                     
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,{"msg":"No users for this event"});
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
         res.json(500,{"msg":"Error"});
       }
    });
*/


     //obtiene un establecimiento por su id
    app.get("/establishment/:id/:user", function(req,res)
    {
       var resposta;
           //id del usuario
     if (!isNaN(req.params.id)){
        var id = req.params.id;

    }

     if (!isNaN(req.params.user)){
        var user= req.params.user;

    }
   
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelEstablishment.getUser(id,user,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];        
         res.json(500,data);
       }
    });


//obtiene los datos de contacto por su id
    app.get("/establishment/contact/:id", function(req,res)
    {
       var resposta;      
           //id del usuario
     if (!isNaN(req.params.id)){
        var id = req.params.id;        
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {



            ModelEstablishment.getContact(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

 app.get("/service/:id", function(req,res)
    {
       var resposta;
           //id del usuario
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))

       if(id !=null)
       {

            ModelEstablishment.getServices(id,function(error, data)              
            {        
               //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                  
                    res.json(200,data);
                    
                    
                   
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });                         
        
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

     //obtiene  los establecimientos cercanos por ciudad o CP
    app.get("/near/:nombre", function(req,res)
    {
        //Evitamos las busquedas por CP
       if (isNaN(req.params.nombre)){
        var nombre= req.params.nombre;
    }
    

        //si existen locales los mostramos
       if(nombre !=null)
       {
            ModelNear.getNear(nombre,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });


    //Filtra los locales por el rango de precios indicados.
    app.get("/filter/:filterID/:id", function(req,res)
    {
        

        var filterID = req.params.filterID;        
        var id = req.params.id;
    

        //si existen locales los mostramos
       if((filterID !=null)&&(id !=null))
       {
            ModelFilters.getFilter(filterID,id,function(error, data)
            {
                //si  existen locales lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

   
 


    //Filtra los locales por el tipo de servicio.
    app.get("/filters/service/:service/:id", function(req,res)
    {
        
        var service = req.params.service;
        var id = req.params.id;
    

        //si existen locales los mostramos
       if((service !=null)&&(id !=null))
       {
            ModelFilters.getService(service,id,function(error, data)
            {
                //si  existen locales lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

    //Filtra los locales por el tipo de servicio.
    app.get("/filters/open/:id", function(req,res)
    {
        
        
        var id = req.params.id;
    

        //si existen locales los mostramos
       if(id !=null)
       {
            ModelFilters.getOpen(id,function(error, data)
            {
                //si  existen locales lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

       //Filtra los locales por que tienen platos apropiados.
    app.get("/filters/appropiate/:appropiate/:id", function(req,res)
    {
        
        var appropiate=req.params.appropiate;
        var id = req.params.id;
    

        //si existen locales los mostramos
       if((appropiate!=null)&&(id !=null))
       {
            ModelFilters.getAppropiate(appropiate,id,function(error, data)
            {
                //si  existen locales lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });


   //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud
    app.get("/near/:lat/:lon/:km", function(req,res)
    {
        

        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelNear.getGeo(lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

      //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrapos por precio
    app.get("/filters/geoPrice/:priceID/:lat/:lon/:km", function(req,res)
    {
        
        var priceID=req.params.priceID;
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((priceID !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoPrice(priceID,lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

   //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrados por tipo
    app.get("/filters/geoType/:typeRes/:lat/:lon/:km", function(req,res)
    {
        
        var typeRes=req.params.typeRes;        
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((typeRes !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoType(typeRes,lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

      //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrados por cocina
    app.get("/filters/geoCuisine/:cuisine/:lat/:lon/:km", function(req,res)
    {
        
        var cuisine=req.params.cuisine;        
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((cuisine !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoCuisine(cuisine,lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

  //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrados por servicios
    app.get("/filters/geoService/:service/:lat/:lon/:km", function(req,res)
    {
        
        var service=req.params.service;        
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((service !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoService(service,lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                    data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
          data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

  //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrados con productos para cel�acos
    app.get("/filters/geoAppropiate/:appropiate/:lat/:lon/:km", function(req,res)
    {
        
        var appropiate=req.params.appropiate;        
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((appropiate !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoAppropiate(appropiate,lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

  

    
  //obtiene  los establecimientos cercanos a trav�s de la lontitud y longitud y filtrados por tipo
    app.get("/filters/geoOpen/:lat/:lon/:km", function(req,res)
    {
        
         
        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((typeRes !=null)&&(lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelFilters.getGeoOpen(lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
    

//obtiene  ofertas por su ciudad(con favorito)
    app.get("/offers/:ciudad/:user", function(req,res)
    {
        
     if (isNaN(req.params.ciudad)){
        var ciudad = req.params.ciudad;
    }
    if (!isNaN(req.params.user)){
        var user = req.params.user;
    }
   
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(ciudad !=null)
       {
            ModelUser.getUser(ciudad,user,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
     

//obtiene  ofertas por su ciudad(sin favorito)
    app.get("/offers/:ciudad", function(req,res)
    {
        
     if (isNaN(req.params.ciudad)){
        var ciudad = req.params.ciudad;
    }
    
   
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(ciudad !=null)
       {
            ModelUser.getUs(ciudad,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

  //Filtro avanzado
    app.post("/offers/establishments", function(req,res)
    {
        //creamos un objeto con los datos a insertar
        var userData = {            
            establishments : req.body.establishments          
        };
        ModelUser.getOtherEstablishments(userData.establishments,function(error, data)
        {
           //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            
            
        });
    })
 

     //obtiene  los menus de un establecimiento
    app.get("/menu/:id", function(req,res)
    {
        
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(id !=null)
       {
            ModelMenu.getUser(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
     
          //obtiene  el contenido del menu por su id
    app.get("/menu/content/:id", function(req,res)
    {
        
     if (!isNaN(req.params.id)){
        var id = req.params.id;
    }
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(id !=null)
       {
            ModelMenu.getMenu(id,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
     

//obtiene eventos por su ciudad
    app.get("/events/:ciudad/:user", function(req,res)
    {
        
     if (isNaN(req.params.ciudad)){
        var ciudad = req.params.ciudad;
    }

     
   
        //solo actualizamos si la id es un n�mero
       //if(isNaN(nombre))
       if(ciudad !=null)
       {
            ModelEvents.getUser(ciudad,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

/*
 //obtiene  los cercanos a traves de la latitud y longitud
    app.get("/events/:lat/:lon/:km", function(req,res)
    {
        

        var lat = req.params.lat;
        var lon = req.params.lon;
        var km = req.params.km;
    

        //si existen locales los mostramos
       if((lat !=null)&&(lon !=null)&&(km !=null))
       {
            ModelEvents.getGeo(lat,lon,km,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
*/
   

  //Filtro avanzado
    app.post("/GeoEvents", function(req,res)
    {
        //creamos un objeto con los datos a insertar
        var userData = {
            lat : req.body.lat,
            lon : req.body.lon,
            km : req.body.km,
            user: req.body.user_id,
            city : req.body.city
         
          
        };
        ModelEvents.getGeo(userData,function(error, data)
        {
           //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            
            
        });
    })

     
 //obtiene los eventos favoritos del usuario
    app.get("/event/favs/:id", function(req,res)
    {
            
        var id = req.params.id;
        var typeID=2;
    
       if(id !=null)
       {
            ModelFav.getFav(id,typeID,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
     
   //obtiene los eventos favoritos del usuario
    app.get("/offer/favs/:id", function(req,res)
    {
            
        var id = req.params.id;
        var typeID=3;
    
       if(id !=null)
       {
            ModelFav.getFav(id,typeID,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });

    //obtiene los eventos favoritos del usuario
    app.get("/establishments/favs/:id", function(req,res)
    {
            
        var id = req.params.id;
        var typeID=1;
    
       if(id !=null)
       {
            ModelFav.getFav(id,typeID,function(error, data)
            {
                //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            });
        }
        //si hay alg�n error
       else
       {
        data=[{"msg":"0"}];
         res.json(500,data);
       }
    });
        //utilizamos el verbo delete para eliminar un usuario
    app.get("/favs/delete/:id", function(req,res)
    {
        //id del ecvento a eliminar
        var id = req.params.id;
       
        ModelFav.deleteUser(id,function(error, data)
        {
            if(data && data.msg === "deleted" || data.msg === "notExist")
            {
              data=[{"msg":"1"}];
                 res.json(200,data);
            }
            else
            {
              data=[{"msg":"0"}];
                res.json(500,data);
            }
        });
    });
/*

      //utilizamos el verbo delete para eliminar un evento que va a ir el usuario
    app.get("/wanttogo/delete/:id", function(req,res)
    {
        //id del evento a ir a eliminar
        var id = req.param('id');
        ModelGo.deleteUser(id,function(error, data)
        {
            if(data && data.msg === "deleted" || data.msg === "notExist")
            {
                res.json(200,data);
            }
            else
            {
                res.json(500,{"msg":"Error"});
            }
        });
    });
*/

 //Filtro avanzado
    app.post("/filterAdv", function(req,res)
    {
        //creamos un objeto con los datos a insertar
        var userData = {
            tipoRest : req.body.establishmentType,
            cocina : req.body.establishmentCuisine,
            precio : req.body.establishmentPrice,
            apropiado : req.body.establishmentAppropiate,
            ciudad : req.body.establishmentCity,
            alt_actividad : req.body.establishmentAlt_activity,
            abierto: req.body.establishmentisOpen
          
        };
        ModelFilterAdvanced.getFilterAdvanced(userData,function(error, data)
        {
           //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            
            
        });
    });


 //Filtro avanzado
    app.post("/filterAdvGeo", function(req,res)
    {
        //creamos un objeto con los datos a insertar
        var userData = {
            tipoRest : req.body.establishmentType,
            cocina : req.body.establishmentCuisine,
            precio : req.body.establishmentPrice,
            apropiado : req.body.establishmentAppropiate,
            lat :   req.body.lat,
            lon :  req.body.lon,
            km :  req.body.km,
            abierto: req.body.establishmentisOpen
          
        };
        ModelFilterAdvanced.getFilterAdvancedGeo(userData,function(error, data)
        {
           //si el usuario existe lo mostramos en formato json
                if (typeof data !== 'undefined' && data.length > 0)
                {
                    res.json(200,data);
                }
                //en otro caso mostramos una respuesta conforme no existe
                else
                {
                  data=[{"msg":"0"}];
                    res.json(404,data);
                }
            
            
        });
    });

 //Inserta un favorito
    app.post("/favs/insert", function(req,res)
    {
      var date = new Date();
        //creamos un objeto con los datos a insertar
        var userData = {
            type : req.body.type,
            user_id : req.body.user_id,
            type_id:req.body.type_id,
            fav_date: date
        };
        ModelFav.insertUser(userData,function(error, data)
        {
            //data=[{"msg":"1"}];
              res.json(200,data);
            
            
        });
    });

//Actualiza un usuario
    app.post("/updateUser", function(req,res)
    {
      
        //creamos un objeto con los datos a insertar
        var userData = {            
            user_id : req.body.user_id,
            user_first_name:req.body.user_first_name,
            user_last_name:req.body.user_last_name
        };
        ModelUserData.updateUser(userData,function(error, data)
        {
            //data=[{"msg":"1"}];
              res.json(200,data);
            
            
        });
    });



         //Inserta un comentario
    app.post("/reviews/insert", function(req,res)
    {
      var date = new Date();
        //creamos un objeto con los datos a insertar del comentario
        var userData = {
            establishment_id : req.body.establishment_id,
            user_id : req.body.user_id,
            comment : req.body.comment,
            delivery_date : date,
            vote    : req.body.vote
        };
        ModelReviews.insertUser(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
           
        });
    });

            //Borra un comentario
    app.post("/reviews/delete", function(req,res)
    {
        //creamos un objeto con los datos a insertar del comentario
        var userData = {
            
            user_id : req.body.user_id,            
            comment_id   : req.body.comment_id
        };
        ModelReviews.deleteUser(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
           
        });
    });

     //Numero de visitas al establecimiento
    app.post("/hit/establishment", function(req,res)
    {
      var fecha = new Date();
     
            //creamos un objeto con los datos a insertar del usuario
        var userData = {
            hit_type : "establishment",
            hit_type_id: req.body.establishment_id,  
            count:1,
            hit_date: fecha,      
        };
        ModelEstablishment.insertvisit(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
            
          
        });
    });

        //Numero de visitas al evento
    app.post("/hit/event", function(req,res)
    {
      var fecha = new Date();

            //creamos un objeto con los datos a insertar del usuario
        var userData = {
            hit_type : "event",
            hit_type_id: req.body.event_id,  
            count:1,
            hit_date: fecha      
        };
        ModelEvents.insertvisit(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
            
          
        });
    });

           //Numero de visitas a la oferta
    app.post("/hit/offer", function(req,res)
    {
      var fecha = new Date();
      
            //creamos un objeto con los datos a insertar del usuario
        var userData = {
            hit_type : "offer",
            hit_type_id: req.body.offer_id, 
            count:1, 
            hit_date: fecha     
        };
        ModelEvents.insertvisit(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
            
          
        });
    });


       //Numero de visitas al menu
    app.post("/hit/menu", function(req,res)
    {
      var fecha = new Date();
      var year = fecha.getFullYear();
      var month= fecha.getMonth()+1;
      var day=fecha.getDate();
            //creamos un objeto con los datos a insertar del usuario
        var userData = {
            hit_type : "menu",
            hit_type_id: req.body.menu_id,  
            hit_date_year: year,
            hit_date_month:month,
            hit_date_day:day        
        };
        ModelEvents.insertvisit(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
            
          
        });
    });


           //Numero de visitas al establecimiento
    app.post("/hit/product", function(req,res)
    {
      var fecha = new Date();
      var year = fecha.getFullYear();
      var month= fecha.getMonth()+1;
      var day=fecha.getDate();
            //creamos un objeto con los datos a insertar del usuario
        var userData = {
            hit_type : "product",
            hit_type_id: req.body.product_id,  
            hit_date_year: year,
            hit_date_month:month,
            hit_date_day:day        
        };
        ModelEvents.insertvisit(userData,function(error, data)
        {
            data=[{"msg":"1"}];
                res.json(200,data);
            
          
        });
    });

/*
     //Inserta un evento al que se va a ir
    app.post("/wanttogo/insert", function(req,res)
    {
        //creamos un objeto con los datos a insertar del usuario
        var userData = {
            event_id : req.body.event_id,
            user_id : req.body.user_id,
        };
        ModelGo.insertUser(userData,function(error, data)
        {
            //si el evento a ir se ha insertado correctamente mostramos su info
            if(data && data.insertId)
            {
                res.json(200,data);
            }
            else
            {
                res.json(500,data);
            }
        });
    });
*/
   
}