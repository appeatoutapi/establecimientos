
     //llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'real'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};
 

 
//obtenemos los platos de la carta
userModel.getUser = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            //var sql="SELECT * FROM PRODUCT WHERE PRODUCT.CATEGORY="+connection.escape(id)+"  GROUP BY PRODUCT.PRODUCT_ID ORDER BY PRODUCT.CATEGORY";

         var sql="SELECT PRODUCT.PRODUCT_ID,PRODUCT.NAME,PRODUCT.IMAGE,PRODUCT.DESCRIPTION,PRODUCT.PRICE,PRODUCT.HIDDEN,PRODUCT.CATEGORY,PRODUCT.COELIACS,PRODUCT.DIABETICS,PRODUCT.ESTABLISHMENT_ID,PRODUCT.VEGANS,PRODUCT.VEGETARIANS, PRODUCT.HALAL,PRODUCT.KOSHER,PRODUCT.ALCOHOL,PRODUCT.PRODUCT_INGREDIENTS,PRODUCT.OWNER_ID,PRODUCT_COMPLEMENT.TYPE_COMPLEMENT,PRODUCT_COMPLEMENT.PRICE_COMPLEMENT,PRODUCT_SIZE.TYPE_SIZE, PRODUCT_SIZE.PRICE_SIZE,PRODUCT.GLUTEN,PRODUCT.CRUSTACEANS,PRODUCT.EGGS,PRODUCT.FISH,PRODUCT.PEANUTS,PRODUCT.SOYA,PRODUCT.DAIRY,PRODUCT.NUTS,PRODUCT.CELERY,PRODUCT.MUSTARD,PRODUCT.SESAME,PRODUCT.SULPHUR,PRODUCT.MOLLUSCS,PRODUCT.LUPINS FROM PRODUCT LEFT JOIN PRODUCT_COMPLEMENT ON PRODUCT_COMPLEMENT.PRODUCT_ID=PRODUCT.PRODUCT_ID LEFT JOIN PRODUCT_SIZE ON PRODUCT_SIZE.PRODUCT_ID=PRODUCT.PRODUCT_ID WHERE PRODUCT.CATEGORY="+connection.escape(id)+" ORDER BY PRODUCT.CATEGORY";
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}

/* PRUEBA DE AÑADIR MANUALMENTE ROWS
//obtenemos los platos de la carta
userModel.getUserPro = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
           var sql="SELECT * FROM PRODUCT WHERE PRODUCT.CATEGORY="+connection.escape(id)+"  GROUP BY PRODUCT.PRODUCT_ID ORDER BY PRODUCT.CATEGORY";

        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                    for(i=0;i<row.length;i++){

              var sqlSize="SELECT * FROM PRODUCT_SIZE WHERE PRODUCT_SIZE.PRODUCT_ID="+row[i].PRODUCT_ID;  
                  connection.query(sqlSize, function(err, rowSize, fields) 
        {
                    if(rowSize){
                        append row[i];

                    }


          }
                callback(null, row);
            }
        });

   

    }
}
}
*/

//obtenemos las categorías de un establecimiento
userModel.getCategories = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            var sql="SELECT PRODUCT_CATEGORIES.CATEGORY_ID, PRODUCT_CATEGORIES.CATEGORY_NAME FROM PRODUCT_CATEGORIES,ESTABLISHMENT WHERE PRODUCT_CATEGORIES.ESTABLISHMENT_ID=ESTABLISHMENT.ESTABLISHMENT_ID AND ESTABLISHMENT.ESTABLISHMENT_ID="+connection.escape(id);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}




   //exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;