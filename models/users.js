//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql');
var fs = require('fs');



//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'real'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};

 

//obtenemos los datos del usuario
userModel.getUser = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){

        var sql = 'SELECT USER_FIRST_NAME,USER_LAST_NAME FROM USER WHERE USER_ID='  + connection.escape(id)+' LIMIT 1;';
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

    } 
}  
}
        
 //actualiza los datos del usuario
userModel.updateUser = function(userData, callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM USER WHERE USER_ID = ' + userData.user_id;
        connection.query(sqlExists, function(err, row) 
        {
            //si existe la id del usuario a actualizar
            if(row)
            {
                var nombre=userData.user_first_name;
                var apellido=userData.user_last_name;

                if( typeof nombre=="undefined"){nombre=row[0].USER_FIRST_NAME;}
                 if(typeof apellido=="undefined"){apellido=row[0].USER_LAST_NAME;}

                var sql = 'UPDATE USER SET USER_FIRST_NAME="'+nombre+'" ,USER_LAST_NAME="'+apellido+'" WHERE USER_ID = ' + userData.user_id;
                connection.query(sql, function(error, result) 
                {
                    if(error)
                    {
                        throw error;
                    }
                    else
                    {
                        callback(null,[{"msg":"updated"}]);
                    }
                });
            }
            else
            {
                callback(null,[{"msg":"notExist"}]);
            }
        });
    }
}

userModel.updateImage = function(userData, callback)
{
if (connection){  

    var sqlExist="SELECT IMAGE FROM USER WHERE IMAGE IS NOT NULL AND USER_ID="+userData.user_id+" LIMIT 1;";
    connection.query(sqlExist,function(err,res){

         if(err)
            {
                throw error;
            }
            else
            {
             
                if(res.length){
                var old_image= res[0].IMAGE;
                require('fs').unlink('../'+old_image); //local
                // require('fs').unlink('../../resources_planeat_io/users/'+old_image); //server




            }

            }
        });




// Regular expression for image type:
        // This regular image extracts the "jpeg" from "image/jpeg"
        var imageTypeRegularExpression      = /\/(.*?)$/;      

        // Generate random string
        var crypto                          = require('crypto');
        var seed                            = crypto.randomBytes(20);
        var uniqueSHA1String                = crypto
                                               .createHash('sha1')
                                                .update(seed)
                                                 .digest('hex');


                                               
   var user_id = userData.user_id;
   var base64Data = userData.image;
   var imageBuffer= decodeBase64Image(base64Data);
   var userUploadedFeedMessagesLocation = '../'; //local
   //    var userUploadedFeedMessagesLocation = '../../resources_planeat_io/users/'; //server

   var uniqueRandomImageName  = user_id +'_' + uniqueSHA1String;

   // This variable is actually an array which has 5 values,
        // The [1] value is the real image extension
        var imageTypeDetected                = imageBuffer.type.match(imageTypeRegularExpression);

        var userUploadedImagePath            = userUploadedFeedMessagesLocation + uniqueRandomImageName + '.' + imageTypeDetected[1];

       var nameImage = uniqueRandomImageName + "." + imageTypeDetected[1];


                                                // Save decoded binary image to disk
        try
        {
        require('fs').writeFile(userUploadedImagePath, imageBuffer.data,  
                                function() 
                                {
                                 // console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
                                    var sql = 'UPDATE USER SET IMAGE="'+nameImage+'" WHERE USER_ID = ' + userData.user_id +' LIMIT 1;';
                                     connection.query(sql, function(error, result) 
                                    {
                                        if(error)
                                        {
                                            throw error;
                                        }
                                        else
                                        {
                                            callback(null,[{"msg":"updated"}]);
                                        }
                                    });

                                });
        }
        catch(error)
        {
         callback(null,[{"msg":"Error"}]);
        } 
   


   
}

}


 function decodeBase64Image(dataString) 
        {
          var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
          var response = {};

          if (matches.length !== 3) 
          {
            return new Error('Invalid input string');
          }

          response.type = matches[1];
          response.data = new Buffer(matches[2], 'base64');

          return response;
        }



 
//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;