//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'real'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};
 
//obtenemos todas las ciudades
userModel.getUsers = function(callback)
{
    if (connection) 
    {
        connection.query('SELECT * FROM CITY ORDER BY CITY_ID', function(error, rows) {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, rows);
            }
        });
    }
}
 
//obtenemos una ciudad por su nombre
userModel.getUser = function(nombre,callback)
{
    if (connection) 
    {

        if (isNaN(nombre)){
        var sql = 'SELECT * FROM CITY WHERE CITY_NAME LIKE '  + connection.escape(nombre) +' OR CITY_NAME_ALT LIKE '+connection.escape(nombre);
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

    } if (!isNaN(nombre)){
var sql = 'SELECT * FROM CITY WHERE CP REGEXP '  + connection.escape(nombre) ;
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

    }
}  
}
        



//obtenemos las ciudades con establecimientos
userModel.getCities = function(callback)
{
    if (connection) 
    {

        
            var sql = 'SELECT CITY.CITY_ID,CITY.CITY_NAME,CITY.IMAGE_PATH FROM CITY,ESTABLISHMENT WHERE CITY.CITY_ID=ESTABLISHMENT.CITY_ID GROUP BY CITY.CITY_NAME';
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    
}
}

 
//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;