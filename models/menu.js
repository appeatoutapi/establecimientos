     //llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'api'
    }
);

//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};
 
//obtenemos los menus de cada establecimiento
userModel.getUser = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            var sql = 'SELECT MENU.MENU_ID, MENU.NAME AS MENU_NAME,MENU.MIN_PEOPLE,MENU.MIN_AGE,MENU.MAX_AGE,MENU.MENU_DATE,MENU.MENU_TIME,MENU.INCLUDES_BREAD,MENU.INCLUDES_DRINK,MENU.INCLUDES_COFFEE,MENU.EXTRA_DESCRIPTION,MENU.DINNER,MENU.LUNCH, MENU.MAX_PEOPLE, MENU.PRICE AS MENU_PRICE, MENU.MENU_TYPE AS MENU_TYPE,MENU.ALCOHOL,MENU.MONDAY,MENU.TUESDAY,MENU.WEDNESDAY,MENU.THURSDAY,MENU.FRIDAY,MENU.SATURDAY,MENU.SUNDAY,MENU.ALL_WEEK FROM MENU, ESTABLISHMENT WHERE MENU.ESTABLISHMENT_ID = ESTABLISHMENT.ESTABLISHMENT_ID AND ESTABLISHMENT.ESTABLISHMENT_ID ='+connection.escape(id); 
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}
//obtiene  el contenido del menu por su id
userModel.getMenu = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            var sql = 'SELECT PLATE.MENU_ID, PLATE.TYPE AS MENU_CATEGORY, PLATE.NAME AS MENU_PLATE, PLATE.DESCRIPTION AS MENU_CONTENT FROM PLATE, MENU WHERE PLATE.MENU_ID = MENU.MENU_ID AND MENU.MENU_ID ='+connection.escape(id)+' ORDER BY PLATE.TYPE';
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}


 //exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;