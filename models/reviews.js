
     //llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'real'
    }
);

//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};
 

 
//obtenemos las críticas de los usuarios
userModel.getUser = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            var sql = 'SELECT USER.USER_FIRST_NAME,USER.USER_LAST_NAME,USER.IMAGE,USER.USER_ID,COMMENT.COMMENT_ID, COMMENT.COMMENT,COMMENT.DELIVERY_DATE, COMMENT.VOTE FROM USER, COMMENT, ESTABLISHMENT WHERE COMMENT.USER_ID=USER.USER_ID AND COMMENT.ESTABLISHMENT_ID=ESTABLISHMENT.ESTABLISHMENT_ID AND ESTABLISHMENT.ESTABLISHMENT_ID='+connection.escape(id);
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}


//obtenemos las críticas de los usuarios
userModel.getData = function(id,callback)
{
    if (connection) 
    {

        if (!isNaN(id)){
            var sql = 'SELECT USER.USER_FIRST_NAME,USER.USER_LAST_NAME,USER.IMAGE, USER.GENDER, USER.BIRTHDAY, USER.ACTIVE FROM USER WHERE USER.USER_ID='+connection.escape(id);
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}


//añadir un nuevo comentario
userModel.insertUser = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO COMMENT SET ?', userData, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                //devolvemos la última id insertada
                callback(null,{"COMMENT_ID" : result.Comment_id});
            }
        });
    }
}
 

 //Eliminar comentario
userModel.deleteUser = function(userData,callback)
{
    if (connection) 
    {

    var user_id=userData.user_id;
    var comment_id=userData.comment_id;

    var sql='DELETE FROM COMMENT WHERE COMMENT.USER_ID='+user_id+' AND COMMENT.COMMENT_ID='+comment_id+';';
        connection.query(sql, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                //devolvemos la última id insertada
                callback(null,{"msg":"deleted"});
            }
        });
    }
}
 
//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;
