//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'localhost', 
        user: 'root',  
        password: '', 
        database: 'prueba'
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var userModel = {};
 

 
//obtenemos los eventos que quiere ir un usuario
userModel.getUser = function(id,callback)
{
    if (connection) 
    {

        if (isNaN(token)){
            var sql = 'SELECT GO.GO_ID,EVENT.TITLE,EVENT.EVENT_ID FROM EVENT, GO, USER WHERE  GO.EVENT_ID = EVENT.EVENT_ID AND GO.USER_ID = USER.USER_ID AND USER.USER_ID=' +connection.escape(id);
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}
   

 //eliminar un evento que quiere ir el usuario
userModel.deleteUser = function(id, callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM GO WHERE GO_ID = ' + connection.escape(id);
        connection.query(sqlExists, function(err, row) 
        {
            //si existe la id del favorito a eliminar
            if(row)
            {
                var sql = 'DELETE FROM GO WHERE GO_ID = ' + connection.escape(id);
                connection.query(sql, function(error, result) 
                {
                    if(error)
                    {
                        throw error;
                    }
                    else
                    {
                        callback(null,{"msg":"deleted"});
                    }
                });
            }
            else
            {
                callback(null,{"msg":"notExist"});
            }
        });
    }
}



//añadir un evento que quiere ir el usuario
userModel.insertUser = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO `GO` SET ?', userData, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                //devolvemos la última id insertada
                callback(null,{"insertId" : result.insertId});
            }
        });
    }
}
 
 
//ver los usuarios que van a asistir a un evento
userModel.getEvent = function(id,callback)
{
    if (connection) 
    {

        if (isNaN(token)){
            var sql = 'SELECT USER.USER_ID, USER.USER_FIRST_NAME, USER.USER_LAST_NAME, EVENT.EVENT_ID FROM  USER, GO, EVENT, WHERE  GO.USER_ID=USER.USER_ID AND EVENT.EVENT_ID = GO.EVENT_ID AND EVENT.EVENT_ID ='+connection.escape(id);
           
        connection.query(sql, function(error, row) 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                callback(null, row);
            }
        });

   

    }
}
}

//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = userModel;